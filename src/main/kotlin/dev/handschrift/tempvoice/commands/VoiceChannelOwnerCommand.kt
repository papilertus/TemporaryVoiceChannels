package dev.handschrift.tempvoice.commands

import com.papilertus.command.Command
import dev.handschrift.tempvoice.temporaryChannels
import net.dv8tion.jda.api.events.interaction.command.SlashCommandInteractionEvent

open class VoiceChannelOwnerCommand(name: String, description: String): Command(name, description, false) {
    override fun execute(event: SlashCommandInteractionEvent): Boolean {
        val member = event.member
        val voiceState = member!!.voiceState
        if (!voiceState!!.inAudioChannel()) {
            event.reply("You are not in a voice channel!").setEphemeral(true).queue()
            return false
        }
        val currentChannel = voiceState.channel

        if (temporaryChannels.none { it.channelId == currentChannel!!.id }) {
            event.reply("You are not in a temporary channel").setEphemeral(true).queue()
            return false
        }

        val tempChannel = temporaryChannels.filter { it.channelId == currentChannel!!.id }[0]

        if (tempChannel.ownerId != member.id) {
            event.reply("You are not the owner of the voice channel you are in!").setEphemeral(true).queue()
            return false
        }
        return true
    }
}