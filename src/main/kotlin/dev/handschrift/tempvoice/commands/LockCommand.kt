package dev.handschrift.tempvoice.commands

import dev.handschrift.tempvoice.temporaryChannels
import net.dv8tion.jda.api.events.interaction.command.SlashCommandInteractionEvent

class LockCommand : VoiceChannelOwnerCommand("lock", "Locks a temporary channel you own") {
    override fun execute(event: SlashCommandInteractionEvent): Boolean {
        val tempChannel = temporaryChannels.filter { it.channelId == event.member!!.voiceState!!.channel!!.id }[0]
        val currentChannel = event.member!!.voiceState!!.channel

        if (tempChannel.locked) {
            currentChannel!!.manager.removePermissionOverride(event.guild!!.publicRole.idLong).queue()
            event.reply("Your channel is now unlocked").setEphemeral(true).queue()
        } else {
            currentChannel!!.manager.putRolePermissionOverride(event.guild!!.publicRole.idLong, 0, 549792514576).queue()
            event.reply("Your channel is now locked").setEphemeral(true).queue()
        }
        tempChannel.locked = !tempChannel.locked
        return true
    }
}