package dev.handschrift.tempvoice.commands

import dev.handschrift.tempvoice.temporaryChannels
import net.dv8tion.jda.api.events.interaction.command.SlashCommandInteractionEvent
import net.dv8tion.jda.api.interactions.commands.OptionType

class GrantCommand : VoiceChannelOwnerCommand("grant", "Allows user to access a locked channel") {
    init {
        this.commandData.addOption(OptionType.USER, "user", "User to grant the permission", true)
    }

    override fun execute(event: SlashCommandInteractionEvent): Boolean {
        if (super.execute(event)) {

            val user = event.getOption("user")!!.asUser
            val tempChannel = temporaryChannels.filter { it.channelId == event.member!!.voiceState!!.channel!!.id }[0]

            event.guild!!.getVoiceChannelById(tempChannel.channelId)!!.manager.putMemberPermissionOverride(
                user.idLong,
                549792514560,
                0
            ).queue()
            event.reply("The user ${user.asMention} is now allowed to join your channel!").setEphemeral(true).queue()
            return true
        }
        return false
    }
}