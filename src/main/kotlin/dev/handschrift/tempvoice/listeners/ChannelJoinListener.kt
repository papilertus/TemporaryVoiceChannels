package dev.handschrift.tempvoice.listeners

import dev.handschrift.tempvoice.Config
import dev.handschrift.tempvoice.data.TempChannel
import dev.handschrift.tempvoice.temporaryChannels
import net.dv8tion.jda.api.events.guild.voice.GuildVoiceUpdateEvent
import net.dv8tion.jda.api.hooks.ListenerAdapter

class ChannelJoinListener(private val config: Config) : ListenerAdapter() {
    override fun onGuildVoiceUpdate(event: GuildVoiceUpdateEvent) {
        val channel = if (event.channelJoined != null) event.channelJoined else event.channelLeft
        if (event.channelJoined != null) { // check if someone joined
            if (channel!!.id == config.joinChannelId) {
                val audioChannel =
                    event.guild.getCategoryById(config.tempCategoryId)!!.createVoiceChannel(config.channelName)
                        .complete()
                event.guild.moveVoiceMember(event.member, audioChannel).queue()
                audioChannel.manager.putMemberPermissionOverride(event.member.idLong, 549792514576, 0).queue()
                temporaryChannels.add(TempChannel(event.member.id, audioChannel.id))
            }
        } else if (event.channelLeft != null) { // check if someone left
            if (temporaryChannels.any { it.channelId == channel!!.id }) {
                if (channel!!.asVoiceChannel().members.isEmpty()) {
                    channel.delete().queue()
                    temporaryChannels.removeIf { it.channelId == channel.id }
                }
            }
        }
    }
}