package dev.handschrift.tempvoice.data

data class TempChannel(var ownerId: String, val channelId: String, var locked: Boolean = false)
