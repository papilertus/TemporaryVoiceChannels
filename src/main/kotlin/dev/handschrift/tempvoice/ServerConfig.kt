package dev.handschrift.tempvoice

data class ServerConfig(val joinChannelID: String, val serverId: String, val tempCategoryId: String)