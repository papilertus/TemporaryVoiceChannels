package dev.handschrift.tempvoice

import com.papilertus.command.Command
import com.papilertus.gui.contextMenu.ContextMenuEntry
import com.papilertus.plugin.Plugin
import com.papilertus.plugin.PluginData
import dev.handschrift.tempvoice.commands.GrantCommand
import dev.handschrift.tempvoice.commands.LockCommand
import dev.handschrift.tempvoice.data.TempChannel
import dev.handschrift.tempvoice.listeners.ChannelJoinListener
import net.dv8tion.jda.api.hooks.EventListener

private lateinit var config: Config
var temporaryChannels = hashSetOf<TempChannel>()
class TempVoiceChannels: Plugin {
    override fun getCommands(): List<Command> {
        return listOf(
            LockCommand(),
            GrantCommand()
        )
    }

    override fun getContextMenuEntries(): List<ContextMenuEntry> {
        return listOf()
    }

    override fun getListeners(): List<EventListener> {
        return listOf(
            ChannelJoinListener(config)
        )
    }

    override fun onLoad(data: PluginData) {
        config = data.registerConfig()
    }

    override fun onUnload() {

    }
}