package dev.handschrift.tempvoice

data class Config (val joinChannelId: String, val tempCategoryId: String, val perServerConfiguration: Boolean, val channelName: String)